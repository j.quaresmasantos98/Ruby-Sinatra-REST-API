require 'sinatra'
require 'json'

file = File.read(File.expand_path('app/db.json'))

before do
  content_type :json
end

get '/health-check' do
  { message: 'Sinatra', version: '1.0' }.to_json
end

get '/countries' do
  data = JSON.parse(file)

  data.to_json
end

get '/countries/:id' do |id|
  data = JSON.parse(file)

  final_data = data.select { |country| country['id'] == id.to_i }.first

  final_data.to_json
end